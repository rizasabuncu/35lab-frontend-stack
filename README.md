# 35Lab Frontend Stack #

### Requirements ###
* [Node.js]
* [npm]
* [gulp.js]


### Requirements Installation ###
```
#!bash

download and install node.js
$ sudo npm install -g npm
$ sudo npm install -g gulp
```


### Project Installation ###
In project directory:
```
#!bash

$ npm install
$ gulp (production)
$ gulp dev (webserver)
```

[Node.js]: http://nodejs.org
[npm]: http://npmjs.com
[gulp.js]: http://gulpjs.com
